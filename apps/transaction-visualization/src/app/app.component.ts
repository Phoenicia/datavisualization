import { Component } from '@angular/core';
import * as augustData from '../assets/Aug2016.json';
import * as julyData from '../assets/Jul2016.json';
import * as junData from '../assets/Jun2016.json';
import { ITransaction } from './models/transaction.interface';
import { TransactionAdapter } from './models/transaction-adapter';
import { ITransactionDto } from './models/transaction-dto.interface';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dataSet = [];


  constructor(private transactionAdapter: TransactionAdapter) {
    this.dataSet = this.parseDataSet([...[...junData], ...[...julyData], ...[...augustData]]);
  }

  parseDataSet(dataSet: ITransactionDto[]): ITransaction[] {
    return dataSet.map((transactionDto) => {
      return this.transactionAdapter.serialize(transactionDto);
    });
  }

}
