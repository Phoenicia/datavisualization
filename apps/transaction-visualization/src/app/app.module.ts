import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TabularViewModule } from './tabular-view';
import { MatTabsModule } from '@angular/material';
import { DailyTransactionsModule } from "./daily-transactions";
import { ExpensePerTypeModule } from "./expense-per-type";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TabularViewModule,
    MatTabsModule,
    DailyTransactionsModule,
    ExpensePerTypeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
