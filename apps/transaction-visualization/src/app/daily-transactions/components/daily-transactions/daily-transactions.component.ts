import { Component, Input } from '@angular/core';
import { ITransaction } from "../../../models/transaction.interface";
import { isNil, sortBy, toPairs, pipe, prop, transpose } from "ramda";
import { ILineChartDataset } from "@codingAssignment/charts";

const dateValueToLabelsAndValues =
  pipe(
    toPairs,
    sortBy(prop('0')),
    transpose
  );

@Component({
  selector: 'app-daily-transactions',
  templateUrl: './daily-transactions.component.html',
  styleUrls: ['./daily-transactions.component.scss']
})
export class DailyTransactionsComponent {
  labels = [];
  data: ILineChartDataset[] = [];

  @Input() set dataSet(data: ITransaction[]){
    const labelsAndValues = dateValueToLabelsAndValues(this.transactionsToDailyTransactions(data));
    this.labels = labelsAndValues[0].map((date) => new Date(+date).toLocaleDateString());
    this.data = [{data: labelsAndValues[1], label: 'Number of Transactions',
      borderColor: ['#3e95cd'], backgroundColor: ['#3e95cd']}];
  };

  constructor() {

  }

  transactionsToDailyTransactions(dataSet: ITransaction[]) {
    const totalSpentByDay = {};
    dataSet.forEach((transaction) => {
      totalSpentByDay[transaction.date.getTime()] = (isNil(totalSpentByDay[transaction.date.getTime()]))
        ? 0 : totalSpentByDay[transaction.date.getTime()] + 1;
    });

    return totalSpentByDay;
  }
}
