import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DailyTransactionsComponent } from './components/daily-transactions/daily-transactions.component';
import { BarChartModule, LineChartModule } from '@codingAssignment/charts';

const components = [DailyTransactionsComponent];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    LineChartModule,
    BarChartModule
  ],
  exports: [...components]
})
export class DailyTransactionsModule { }
