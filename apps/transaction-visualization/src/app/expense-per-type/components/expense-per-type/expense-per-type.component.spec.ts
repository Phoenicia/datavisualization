import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensePerTypeComponent } from './expense-per-type.component';

describe('ExpensePerTypeComponent', () => {
  let component: ExpensePerTypeComponent;
  let fixture: ComponentFixture<ExpensePerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensePerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensePerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
