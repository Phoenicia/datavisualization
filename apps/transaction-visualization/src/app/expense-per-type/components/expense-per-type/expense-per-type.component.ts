import { Component, Input } from '@angular/core';
import { IBarChartDataset } from "@codingAssignment/charts";
import { ITransaction } from "../../../models/transaction.interface";
import { isNil, sortBy, toPairs, pipe, prop, transpose, reverse, slice } from "ramda";

const dateValueToLabelsAndValues =
  pipe(
    toPairs,
    sortBy(prop('1')),
    reverse,
    slice(0,7),
    transpose
  );

@Component({
  selector: 'app-expense-per-type',
  templateUrl: './expense-per-type.component.html',
  styleUrls: ['./expense-per-type.component.scss']
})
export class ExpensePerTypeComponent {
  labels = [];
  data: IBarChartDataset = { data: [], label: '', backgroundColor: [] };

  @Input() set dataSet(data: ITransaction[]){
    const labelsAndValues = dateValueToLabelsAndValues(this.transactionsToDailyTransactions(data));
    this.labels = labelsAndValues[0];
    this.data = {
      data: labelsAndValues[1],
      label: 'Total of Transactions',
      backgroundColor: ['#3e95cd', '#8e5ea2','#3cba9f','#e8c3b9','#c45850','#3e95cd']};
  };

  constructor() {

  }

  transactionsToDailyTransactions(dataSet: ITransaction[]) {
    const totalSpentByDay = {};
    dataSet.forEach((transaction) => {
      totalSpentByDay[transaction.expenseType] = (isNil(totalSpentByDay[transaction.expenseType]))
        ? transaction.amount : totalSpentByDay[transaction.expenseType] + transaction.amount;
    });

    return totalSpentByDay;
  }
}
