import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpensePerTypeComponent } from './components/expense-per-type/expense-per-type.component';
import { BarChartModule } from "@codingAssignment/charts";

const components = [ExpensePerTypeComponent];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    BarChartModule
  ],
  exports: [...components]
})
export class ExpensePerTypeModule { }
