import { Injectable } from '@angular/core';
import { ITransactionDto } from './transaction-dto.interface';
import { ITransaction } from './transaction.interface';
import { pathOr, ifElse, isNil } from 'ramda';

@Injectable({
  providedIn: 'root'
})
export class TransactionAdapter {
  serialize(transactionDto: ITransactionDto): ITransaction {
    return {
      departmentFamily: pathOr('', ['Department Family'], transactionDto),
      entity: pathOr('', ['Entity'], transactionDto),
      date: ifElse(isNil, () => ({}), this.stringToDate)(transactionDto.Date),
      expenseType: pathOr('', ['Expense Type'], transactionDto),
      expenseArea: pathOr('', ['Expense Area'], transactionDto),
      supplier: pathOr('', ['Supplier'], transactionDto),
      transactionNumber: pathOr('', ['Transaction Number'], transactionDto),
      amount: pathOr('', ['Amount'], transactionDto),
      invoiceCurrencyUnit: pathOr('', ['Invoice Currency Unit'], transactionDto)
    };
  }

  private stringToDate(date: string) : Date {
    const dateParts = date.split('/');

    return new Date(+dateParts[2], +dateParts[1] - 1, +dateParts[0])
  }
}
