export interface ITransactionDto {
  'Department Family': string;
  'Entity': string;
  'Date': string;
  'Expense Type': string;
  'Expense Area': string;
  'Supplier': string;
  'Transaction Number': number;
  'Amount': number;
  'Invoice Currency Unit': string;
}
