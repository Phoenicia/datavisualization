export interface ITransaction {
  'departmentFamily': string;
  'entity': string;
  'date': Date;
  'expenseType': string;
  'expenseArea': string;
  'supplier': string;
  'transactionNumber': number;
  'amount': number;
  'invoiceCurrencyUnit': string;
}
