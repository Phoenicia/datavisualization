import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ITransaction} from '../../../models/transaction.interface';

@Component({
  selector: 'app-tabular-view',
  templateUrl: './tabular-view.component.html',
  styleUrls: ['./tabular-view.component.scss']
})
export class TabularViewComponent implements OnInit {
  displayedColumns = ['departmentFamily', 'entity', 'date', 'expenseType', 'expenseArea', 'supplier', 'transactionNumber', 'amount',
    'invoiceCurrencyUnit'];
  _dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input()
  set dataSource(data: ITransaction[]) {
    this._dataSource = new MatTableDataSource<ITransaction>(data);
  }
  get dataSource() {
    return this._dataSource;
  }

  constructor() { }

  ngOnInit(): void {
    this._dataSource.paginator = this.paginator;
    this._dataSource.sort = this.sort;
  }
}
