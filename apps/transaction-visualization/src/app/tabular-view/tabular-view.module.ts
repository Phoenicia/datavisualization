import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabularViewComponent } from './components/tabular-view/tabular-view.component';
import { MatCardModule, MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const components = [TabularViewComponent];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatCardModule
  ],
  exports: [...components]
})
export class TabularViewModule { }
