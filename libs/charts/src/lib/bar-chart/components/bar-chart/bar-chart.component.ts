import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { IBarChartDataset } from "../../models/bar-chart.dataset.interface";

@Component({
  selector: 'chart-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  @ViewChild('canvas') canvas: ElementRef;

  @Input() labels: string[];
  @Input() dataset: IBarChartDataset;

  chart = [];

  constructor() {

  }

  ngOnInit() {
    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'bar',
      data: {
        labels: this.labels,
        datasets: [
          {
            label: this.dataset.label,
            backgroundColor: this.dataset.backgroundColor,
            data: this.dataset.data
          }]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: this.dataset.label
        },
        maintainAspectRatio: false
      }
    });
  }
}
