export * from './bar-chart.module';
export * from './components/bar-chart/bar-chart.component';
export * from './models/bar-chart.dataset.interface';
