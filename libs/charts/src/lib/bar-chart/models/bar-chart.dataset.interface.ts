export interface IBarChartDataset {
  label: string;
  data: number[];
  backgroundColor?: string[];
}
