import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { ILineChartDataset } from "../../models/line-chart.dataset.interface";

@Component({
  selector: 'chart-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  @ViewChild('canvas') canvas: ElementRef;

  @Input() labels: string[];
  @Input() datasets: ILineChartDataset[];

  chart = [];

  constructor() {

  }

  ngOnInit() {
    const datasets = this.datasets.map((dataset) => {
      dataset['fill'] = false;
      return dataset;
    });

    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: datasets,
      },
      options: {
        maintainAspectRatio: false
      }});
  }
}
