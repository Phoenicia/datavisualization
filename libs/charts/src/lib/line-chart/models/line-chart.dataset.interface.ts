export interface ILineChartDataset {
  label: string;
  data: number[];
  borderColor?: string[];
  backgroundColor?: string[];
}
